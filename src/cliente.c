#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>             
#include <stdlib.h>            
#include <stddef.h>            
#include <string.h>             
#include <unistd.h>             
#include <signal.h>             
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

int main( int argc, char *argv[]) {
	int socketfd;
	if(argc != 5){
		printf("Use: ./cliente <ip> <puerto> <ruta_remota> <ruta_local>\n");
		return (-1);
	}
	int port = atoi(argv[2]);
	struct sockaddr_in direccion_servidor;
	memset(&direccion_servidor, 0, sizeof(direccion_servidor));
	direccion_servidor.sin_family = AF_INET;
	direccion_servidor.sin_port = htons(port);		
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[1]) ;
	socketfd = socket(direccion_servidor.sin_family, SOCK_STREAM, 0);
	if(socketfd<0){
	return (-1);
}
	int cntfd = connect(socketfd,(struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor));
	if(cntfd<0){
		printf("Falló conexion");
		
}	
	char get[]="GET,";
	
		
	char buff[3000] =  {0};
	send(socketfd, strcat(get,argv[3]), strlen(get)+strlen(argv[3]), 0);
	char tam_arch[20]= {0};
	recv(socketfd, tam_arch, 20, 0);
	
	int ad,c;
	ad= open(argv[4], O_CREAT | O_WRONLY, 	0666);
	if(ad>0){
		while((c=recv(socketfd, buff, 3000, 0))>0){
			
			write(ad, buff,c);
	}
	}
	close(socketfd);	
	close(ad);	
	return 0; 
	
}

