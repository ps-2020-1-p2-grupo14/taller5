
#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>             
#include <stddef.h>             
#include <string.h>             
#include <unistd.h>             
#include <signal.h>             
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128 
#define QLEN 10 
#define BUFLEN1 5000

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	


void set_cloexec(int fd){
	if(fcntl(fd, F_SETFD, fcntl(fd, F_GETFD) | FD_CLOEXEC) < 0){
		printf("error al establecer la bandera FD_CLOEXEC\n");	
	}
}


//para levantar el servidor
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	int err = 0;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
		
	if(bind(fd, addr, alen) < 0)
		goto errout;
		
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
		
			if(listen(fd, qlen) < 0)
				goto errout;
	}
	return fd;
errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}

int main( int argc, char *argv[]) { 
	struct stat datos_archivo;
	
	int sockfd;
	
	if(argc != 3){
		printf("Use: ./servidor <ip> <puerto>\n");
		return (-1);
	}

	int puerto = atoi(argv[2]);
	
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));

	
	direccion_servidor.sin_family = AF_INET;	
	direccion_servidor.sin_port = htons(puerto);
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[1]);

	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){ 
		printf("Error inicializando servidor\n");	
	}	
	int clfd1 = accept(sockfd, NULL, NULL);	
	int n = 0;
	char buf[BUFLEN] = {0};
	if (( n = recv( clfd1, buf, BUFLEN,0)) < 0) {
			printf("error al leer");
				
	}			
	char *ptr = strtok(buf, ",");
	int x=0;	
	while(x<1){
	ptr = strtok(NULL, ",");
	x++;
	}
	int res = stat(ptr, &datos_archivo);
	if(res<0){
	perror("Error stat\n");
	}
	
	char valor[20]= {0};
	snprintf(valor, 20, "%lu",datos_archivo.st_size);
	send(clfd1, valor,20,0);
	int b;
	
	unsigned char buffer[BUFLEN1]={0};
	int f1 = open(ptr, O_RDONLY, 0444);
	while((b=read(f1,buffer,BUFLEN1))>0){
		send(clfd1, buffer, b, 0);
	}
	close(f1);
	close(clfd1);
	exit( 1); 
}


