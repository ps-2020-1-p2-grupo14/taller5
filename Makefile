CC = gcc -Wall -c
all: bin/cliente bin/servidor

bin/cliente: obj/cliente.o
	gcc $^ -o $@ -lm

obj/cliente.o: src/cliente.c
	$(CC) $^ -o $@


bin/servidor: obj/servidor.o
	gcc $^ -o $@ -lm

obj/servidor.o: src/servidor.c
	$(CC) $^ -o $@


.PHONY: clean
clean:
	rm bin/cliente bin/servidor obj/*.o
